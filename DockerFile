 
FROM openjdk:8
ADD target/springboot-apirest.jar .
ENTRYPOINT ["java", "-jar", "springboot-apirest.jar"]